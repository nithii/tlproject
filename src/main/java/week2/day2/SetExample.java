package week2.day2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SetExample {

	public static void main(String[] args) {
		Set<String> strMob = new HashSet<String>();
		List<String> listMob = new ArrayList<String>();
		strMob.add("Nokia");
		strMob.add("Samsung");
		strMob.add("LG");
		strMob.add("Nokia");
		strMob.add("Motorola");
		
		for (String mob : strMob) {
		System.out.println("*****Mobile Phones**********" +mob);
		}
		listMob.addAll(strMob);
		System.out.println("*****First Mobile Purchased*******"+listMob.get(0));
	}
	
}

