package week2.day2;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap.KeySetView;

//To print all the duplicate characters
public class MapExample {
 public static void main(String[] args) {
	String myName = "Niidyaa ashok";
	char[] ch = myName.toCharArray();
	Map<Character, Integer> nameMap = new HashMap<Character,Integer>();
	for (char c : ch) {
		if(nameMap.containsKey(c)) {
			nameMap.put(c, nameMap.get(c)+1);
			
	    } else {
		nameMap.put(c,1);
	    }
	}
	for(char c:nameMap.keySet()) {
	if(nameMap.get(c)>1) {
		System.out.println("**********Duplicate CHaracters**********" +c+ " Occurs "+nameMap.get(c)+" times");
		//break;
	}
	}
	}
	
	
 }
	

	

